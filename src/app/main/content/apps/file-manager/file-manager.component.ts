import {Component, ElementRef, OnInit, ViewEncapsulation} from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { FileManagerService } from './file-manager.service';
import {FileUploader} from 'ng2-file-upload';
import {HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
const URL = 'path_to_api';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './file-manager.component.html',
    styleUrls    : ['./file-manager.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
    providers    : [FileManagerService]
})
export class FuseFileManagerComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private uploadService: FileManagerService,
                private router: Router){
    }

    ngOnInit()
    {
        this.uploadService.onFileSelected.subscribe(selected => {
            this.selected = selected;
            this.pathArr = selected.location.split('>');
        });
    }
/*
  public uploader:FileUploader = new FileUploader({url: URL});
*/

   /* public uploadFile(): void {
      let files = this.enum.nativeElement.querySelector('#fileInput').file;
      let formData = new FormData();
    /!*  let file = files[0];
*!/
      formData.append('file', files, files.name);
      this.fileManagerService.uploadFiles(formData).subscribe(
        res => console.log(res));
    }
*/

 /* public onChange($event:any):void {
    this.fileUploadService.addToQueue($event.srcElement.files);
  }*/

  selectedFiles: FileList;
  currentFileUpload: File;
  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  upload() {
    this.currentFileUpload = this.selectedFiles.item(0);
    this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event instanceof HttpResponse) {
       // console.log('File is completely uploaded!');
        if (event.body.toString().length === 4)
        {
          alert("File Uploaded Successfully");
          this.router.navigate(['/**']);

        }
        console.log(event.body);
      }
    });
    this.selectedFiles = undefined;
  }
}
