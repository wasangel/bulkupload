import { FuseUtils } from '@fuse/utils';

export class Order
{
  refrenceNo: string;
  fullName: string;
  dateOfLeaving: string;
  basicSalary: string;
  gross_Salary: string;
  department: string;
  reportingPerson: string;

    constructor(order?)
    {
        order = order || {};
        this.refrenceNo = order.refrenceNo || FuseUtils.generateGUID();
        this.fullName = order.fullName || FuseUtils.generateGUID();
        this.dateOfLeaving = order.dateOfLeaving || 0;
        this.basicSalary = order.basicSalary || 0;
        this.gross_Salary = order.gross_Salary || 0;
        this.department = order.department || 0;
        this.reportingPerson = order.reportingPerson || 0;
        }
}
