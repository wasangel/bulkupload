import {Component, OnInit, ViewEncapsulation} from '@angular/core';

import { AnalyticsDashboardService } from './analytics.service';
import { fuseAnimations } from '@fuse/animations';
import { Chart } from 'chart.js';


@Component({
    selector     : 'fuse-analytics-dashboard',
    templateUrl  : './analytics.component.html',
    styleUrls    : ['./analytics.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class FuseAnalyticsDashboardComponent implements OnInit
{
  chart = [];
  chart1 = [];
    widgets: any;
    widget1SelectedYear = '2016';
    widget5SelectedDay = 'today';

    constructor(
        private analyticsDashboardService: AnalyticsDashboardService
    )
    {
        // Get the widgets from the service
       // this.widgets = this.analyticsDashboardService.widgets;

        // Register the custom chart.js plugin
       // this.registerCustomChartJSPlugin();
    }

//     /**
//      * Register a custom plugin
//      */
//     registerCustomChartJSPlugin()
//     {
//         (<any>window).Chart.plugins.register({
//             afterDatasetsDraw: function (chart, easing) {
//                 // Only activate the plugin if it's made available
//                 // in the options
//                 if (
//                     !chart.options.plugins.xLabelsOnTop ||
//                     (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
//                 )
//                 {
//                     return;
//                 }
//
//                 // To only draw at the end of animation, check for easing === 1
//                 const ctx = chart.ctx;
//            /*     console.log(ctx);
// */
//                 chart.data.datasets.forEach(function (dataset, i) {
//                   console.log(chart.data.datasets);
//                     const meta = chart.getDatasetMeta(i);
//                     if ( !meta.hidden )
//                     {
//
//                     /*  console.log(chart.data.datasets);
//                     */    meta.data.forEach(function (element, index) {
//
//                             // Draw the text in black, with the specified font
//                             ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
//                             const fontSize = 13;
//                             const fontStyle = 'normal';
//                             const fontFamily = 'Roboto, Helvetica Neue, Arial';
//                             ctx.font = (<any>window).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
//
//                             // Just naively convert to string for now
//                             const dataString = dataset.data[index].toString() + 'k';
//
//                             // Make sure alignment settings are correct
//                             ctx.textAlign = 'center';
//                             ctx.textBaseline = 'middle';
//                             const padding = 15;
//                             const startY = 24;
//                             const position = element.tooltipPosition();
//                             ctx.fillText(dataString, position.x, startY);
//
//                             ctx.save();
//
//                             ctx.beginPath();
//                             ctx.setLineDash([5, 3]);
//                             ctx.moveTo(position.x, startY + padding);
//                             ctx.lineTo(position.x, position.y - padding);
//                             ctx.strokeStyle = 'rgba(255,255,255,0.12)';
//                             ctx.stroke();
//
//                             ctx.restore();
//                         });
//                     }
//                 });
//             }
//         });
//     }


  ngOnInit() {



    this.analyticsDashboardService.dailyForecast()
      .subscribe(res => {

        let temp_max = res['chartData'].map(res => res.totalRecords);
        let temp_min = res['chartData'].map(res => res.timeInMilisec);
        let alldates = res['chartData'].map(res => res.fileName);

        let weatherDates = [];
        alldates.forEach((res) => {
          let jsdate = new Date(res * 1000);
          weatherDates.push(jsdate.toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric'}))
        });

        this.chart = new Chart('canvas',
          {
          type: 'line',
          data: {
            labels: alldates,
            datasets: [
              {
                data: temp_max,
                borderColor: '#3cba9f',
                fill: false
              },
              {
                data: temp_min,
                borderColor: '#ffcc00',
                fill: false
              },
            ]
          },
          options: {
            legend: {
              display: false
            },
            scales: {
              xAxes: [{
                display: true
              }],
              yAxes: [{
                display: true
              }]
            }
          }
        });

        this.chart1 = new Chart('canvas1',
          {
          type: 'bar',
          data: {
            labels: alldates,
            datasets: [
              {
                data: temp_max,
                borderColor: '#3cba9f',
                fill: false
              },
              {
                data: temp_min,
                borderColor: '#ffcc00',
                fill: false
              },
            ]
          },
          options: {
            legend: {
              display: false
            },
            scales: {
              xAxes: [{
                display: true
              }],
              yAxes: [{
                display: true
              }]
            }
          }
        });

      });
  }
}

