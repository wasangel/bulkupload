export const navigation = [
    {
        'id'       : 'applications',
        'title'    : 'Applications',
        'translate': 'NAV.APPLICATIONS',
        'type'     : 'group',
        'icon'     : 'apps',
        'children' : [
            {
                'id'       : 'dashboards',
                'title'    : 'Dashboards',
                'translate': 'NAV.DASHBOARDS',
                'type'     : 'collapse',
                'icon'     : 'dashboard',
              'url'  : '/apps/dashboards/analytics'
                // 'children' : [
                //     {
                //         'id'   : 'analytics',
                //         'title': 'My Dashboard',
                //         'type' : 'item',
                //         'url'  : '/apps/dashboards/analytics'
                //     }
                //     // {
                //     //     'id'   : 'project',
                //     //     'title': 'Project',
                //     //     'type' : 'item',
                //     //     'url'  : '/apps/dashboards/project'
                //     // }
                // ]
            },

          {
            'id'       : 'file-manager',
            'title'    : 'File Upload',
            'translate': 'NAV.FILE_MANAGER',
            'type'     : 'item',
            'icon'     : 'folder',
            'url'      : '/apps/file-manager'
          },

            {
                'id'       : 'e-commerce',
                'title'    : 'Employee',
                'translate': 'NAV.ECOMMERCE',
                'type'     : 'collapse',
                'icon'     : 'shopping_cart',
                'children' : [



                    {
                        'id'        : 'orders',
                        'title'     : 'Employee List',
                        'type'      : 'item',
                        'url'       : '/apps/e-commerce/orders',
                        'exactMatch': true
                    }

                ]
            }


        ]
    },
    /*{
        'id'      : 'pages',
        'title'   : 'Login',
        'type'    : 'group',
        'icon'    : 'pages',

        'children': [
            {
                'id'      : 'authentication',
                'title'   : 'Login ',
                'type'    : 'collapse',
                'icon'    : 'lock',
              'url'  : '/pages/auth/login',
                'badge'   : {
                    'title': 10,
                    'bg'   : '#525e8a',
                    'fg'   : '#FFFFFF'
                },
                // 'children': [
                //     {
                //         'id'   : 'login',
                //         'title': 'Login',
                //         'type' : 'item',
                //         'url'  : '/pages/auth/login'
                //     },
                //
                // ]
            }



        ]
    },*/
    // {
    //     'id'      : 'user-interface',
    //     'title'   : 'User Interface',
    //     'type'    : 'group',
    //     'icon'    : 'web',
    //     'children': [
    //         {
    //             'id'   : 'forms',
    //             'title': 'Forms',
    //             'type' : 'item',
    //             'icon' : 'web_asset',
    //             'url'  : '/ui/forms'
    //         }
    //     ]
    // },



];
